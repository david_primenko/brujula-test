const ADDED_ACTION = "added";
const REMOVED_ACTION = "removed";

var ListJS = function ({selector = ".listjs", data = [], config = {multiple: true, size: 10}, trans = {
    addItem: `Añadir elemento a la lista (${selector}):`,
    undo: "Deshacer"
}} = {}) {
    let self = null;
    let obj = {
        baseEl: selector,
        listEl: `${selector} .list select`,
        optionEl: `${selector} .list select option`,
        addEl: `${selector} .controls .add`,
        removeEl: `${selector} .controls .remove`,
        undoEl: `${selector} .controls .undo`,
        data: [],
        lastAction: "",
        lastItems : [],
        initialize(params) {
            self = this;
            self.data = params.data;
            self.config = params.config;
            self.trans = params.trans;
            self.renderComponent();
            if (self.config.multiple) {
                self.enableMultipleSelection(self.config.multiple);
            }
            self.refreshList();
        },
        events() {

            document.querySelector(self.addEl).removeEventListener("click", self.addItemHandler)
            document.querySelector(self.addEl).addEventListener("click", self.addItemHandler);

            document.querySelectorAll(self.optionEl).forEach(function(elem) {
                elem.removeEventListener("dblclick", self.dblClickItemHandler);
                elem.addEventListener("dblclick", self.dblClickItemHandler);
            });

            document.querySelector(self.removeEl).removeEventListener("click", self.removeItemsHandler)
            document.querySelector(self.removeEl).addEventListener("click", self.removeItemsHandler);

            document.querySelector(self.undoEl).removeEventListener("click", self.undoHandler)
            document.querySelector(self.undoEl).addEventListener("click", self.undoHandler);
        },
        renderComponent() {
            const html =  `
                <div class="list-cont">
                    <div class="list">
                        <select class="list-control" size="${self.config.size}"></select>
                    </div>
                    <div class="controls">
                        <div class="main">
                            <button class="list-control ctrl-btn add">+</button>
                            <button class="list-control ctrl-btn remove">-</button>
                        </div>
                        <div class="extra">
                            <button class="list-control ctrl-btn undo">${self.trans.undo}</button>
                        </div>
                    </div>
                </div>             
            `;

            document.querySelector(self.baseEl).innerHTML = html;
        },
        enableMultipleSelection() {
            document.querySelector(self.listEl).setAttribute("multiple", "true");
        },
        addItemHandler : function() {
            let item = prompt(self.trans.addItem);
            if (item != null) {
                self.addItem(item);
            }
        },
        removeItemsHandler(ev) {
            ev.preventDefault();

            const options = (self.getList()).childNodes;
            let indexToRemove = [];

            options.forEach((elem, index) => {
                if (elem.selected) {
                    indexToRemove.push(index);
                }
            });

            self.removeItems(indexToRemove);
        },
        dblClickItemHandler(ev) {
            self.removeItems([ev.target.index]);
        },
        undoHandler: function() {
            self.undo();
        },
        addItem(item) {
            self.resetUndoParams();
            self.data.push(item);
            self.lastAction = ADDED_ACTION;
            self.refreshList();
        },
        restoreItemsOnIndex() {
            for (let i = (self.lastItems.length - 1); i >= 0; i--) {
                let val = self.lastItems[i];
                self.data.splice(val.index, 0,val.item)
            }

            self.refreshList();
        },
        removeItems(arrIndex) {

            if (arrIndex.length === 0) {
                return false;
            }

            self.resetUndoParams();

            for (let i = (arrIndex.length - 1); i >= 0; i--) {
                let val = arrIndex[i];
                self.lastItems.push({
                    index: val,
                    item: self.data[val]
                });
                self.data.splice(val,1);
            }

            self.lastAction = REMOVED_ACTION;
            self.refreshList();
        },
        undo() {
            if (self.lastAction === "") {
                return false;
            }

            switch (self.lastAction) {
                case ADDED_ACTION:
                    self.removeItems([(self.data.length - 1)]);
                    break;
                case REMOVED_ACTION:
                    self.restoreItemsOnIndex();
            }

            self.resetUndoParams();
        },
        resetUndoParams() {
            self.lastAction = "";
            self.lastItems = [];
        },
        getList() {
            return document.querySelector(self.listEl);
        },
        clearList() {
            (self.getList()).innerHTML = "";
        },
        fillList() {
            const list = self.getList();

            self.data.forEach((elem) => {
                list.appendChild(self.generateOption(elem));
            });
        },
        generateOption(value) {
            const option = document.createElement("option");
            option.value = value;
            option.innerText = value;
            return option;
        },
        refreshList() {
            self.clearList();
            self.fillList();
            self.events();
        },
    };

    obj.initialize({
        data,
        config,
        trans
    });

    return obj;
};